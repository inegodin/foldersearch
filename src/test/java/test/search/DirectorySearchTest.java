package test.search;

import org.junit.Assert;
import org.junit.Test;
import test.search.match.BytesComparingMatcherTest;

/**
 * Created by inegodin on 05/05/15.
 */
public class DirectorySearchTest {

    @Test
    public void testDirectoryIsNull() throws Exception {
        DirectorySearch directorySearch = new DirectorySearch(1, " ");
        int matches = directorySearch.searchIn(null);
        Assert.assertEquals(0, matches);
    }

    @Test
    public void testDirectoryDoesntExist() throws Exception {
        DirectorySearch directorySearch = new DirectorySearch(1, " ");
        int matches = directorySearch.searchIn("");
        Assert.assertEquals(0, matches);
    }

    @Test
    public void testDirectoryIsFile() throws Exception {
        DirectorySearch directorySearch = new DirectorySearch(1, "ship");
        int matches = directorySearch.searchIn(BytesComparingMatcherTest.WHITEFLAG_TXT);
        Assert.assertEquals(0, matches);
    }

    @Test
    public void testDirectoryIsEmpty() throws Exception {
        DirectorySearch directorySearch = new DirectorySearch(1, "ship");
        int matches = directorySearch.searchIn("src/main/resources");
        Assert.assertEquals(0, matches);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testPatternNull() {
        new DirectorySearch(1, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testPatternEmpty() {
        new DirectorySearch(1, "");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testThreadsCountLessThanOne() {
        new DirectorySearch(0, " ");
    }
}