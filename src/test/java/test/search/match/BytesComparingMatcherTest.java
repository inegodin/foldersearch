package test.search.match;

import org.junit.BeforeClass;
import org.junit.Test;

import java.io.RandomAccessFile;

import static org.junit.Assert.assertTrue;

/**
 * Created by inegodin on 03/05/15.
 */
public class BytesComparingMatcherTest {

    public static final String WHITEFLAG_TXT = "src/test/resources/whiteflag.txt";
    private static PatternMatcher classToTest;

    @BeforeClass
    public static void setUp() throws Exception {
        classToTest = new BytesComparingMatcher("ship");
    }

    @Test
    public void testMatch() throws Exception {
        RandomAccessFile file = null;
        int matches;
        try {
            file = new RandomAccessFile(WHITEFLAG_TXT, "r");
            byte[] bytes = new byte[(int) file.length()];
            file.read(bytes);
            matches = classToTest.match(bytes, 0, WHITEFLAG_TXT);
        } finally {
            if (file != null) {
                file.close();
            }
        }
        assertTrue("Matches count should equals to 5", matches == 5);
    }
}