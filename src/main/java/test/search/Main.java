package test.search;

/**
 * Created by inegodin on 03/05/15.
 */
public class Main {
    public static void main(String[] args) {
        if (args.length != 3) {
            System.err.println("please specify all program arguments");
            return;
        }
        Integer threads;
        try {
            threads = Integer.valueOf(args[0]);
        } catch (NumberFormatException e) {
            System.err.println("First argument must be a number");
            return;
        }
        Search search = new DirectorySearch(threads, args[1]);
        int matchesCount = search.searchIn(args[2]);
        System.out.println("Found total " + matchesCount + " match(es) in " + search.getMatchedFilesCount() + " file(s)");
        System.out.println("Done with " + threads + " thread(s), uptime " + search.getUptime() + " ms");
    }
}
