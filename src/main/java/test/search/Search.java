package test.search;

/**
 * Created by inegodin on 03/05/15.
 */
public interface Search {

    int searchIn(String directory);

    int getMatchedFilesCount();

    long getUptime();
}
