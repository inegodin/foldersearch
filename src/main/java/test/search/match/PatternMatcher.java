package test.search.match;

/**
 * Created by inegodin on 03/05/15.
 */
public interface PatternMatcher {

    int match(byte[] bytes, long offset, String absolutePath);

    byte[] getPattern();
}
