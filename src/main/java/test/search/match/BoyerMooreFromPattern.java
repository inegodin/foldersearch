package test.search.match;

import java.nio.charset.Charset;

/**
 * Boyer-Moore search method. Copied from java.util.regex.Pattern.java
 */
public class BoyerMooreFromPattern {

    private final int[] bcs; // Bad Character Shift table
    private final int[] gss; // Good Suffix Shift table
    private final byte[] pattern;

    private BoyerMooreFromPattern(int[] bcs, int[] gss, byte[] pattern) {
        this.bcs = bcs;
        this.gss = gss;
        this.pattern = pattern;
    }

    public static BoyerMooreFromPattern compilePattern(String patternString) {
        int i, j;
        byte[] pattern = patternString.getBytes(Charset.forName("UTF-8"));
        int[] bcs = new int[128];
        int[] gss = new int[pattern.length];
        // Precalculate part of the bad character shift
        // It is a table for where in the pattern each
        // lower 7-bit value occurs
        for (i = 0; i < pattern.length; i++) {
            bcs[pattern[i] & 0x7F] = i + 1;
        }

        // Precalculate the good suffix shift
        // i is the shift amount being considered
        NEXT:
        for (i = pattern.length; i > 0; i--) {
            // j is the beginning index of suffix being considered
            for (j = pattern.length - 1; j >= i; j--) {
                // Testing for good suffix
                if (pattern[j] == pattern[j - i]) {
                    // src[j..len] is a good suffix
                    gss[j - 1] = i;
                } else {
                    // No match. The array has already been
                    // filled up with correct values before.
                    continue NEXT;
                }
            }
            // This fills up the remaining of optoSft
            // any suffix can not have larger shift amount
            // then its sub-suffix. Why???
            while (j > 0) {
                gss[--j] = i;
            }
        }
        // Set the guard value because of unicode compression
        gss[pattern.length - 1] = 1;

        return new BoyerMooreFromPattern(bcs, gss, pattern);
    }

    public int match(byte[] buf, int off, int len) {
        int last = len - pattern.length;

        // Loop over all possible match positions in text
        NEXT:
        while (off <= last) {
            // Loop over pattern from right to left
            for (int j = pattern.length - 1; j >= 0; j--) {
                byte ch = buf[off + j];
                if (ch != pattern[j]) {
                    // Shift search to the right by the maximum of the
                    // bad character shift and the good suffix shift
                    off += Math.max(j + 1 - bcs[ch & 0x7F], gss[j]);
                    continue NEXT;
                }
            }
            // Entire pattern matched starting at off
            return off;
        }
        return -1;
    }

    public byte[] getPattern() {
        return pattern;
    }
}
