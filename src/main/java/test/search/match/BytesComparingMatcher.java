package test.search.match;

/**
 * Created by inegodin on 03/05/15.
 */
public class BytesComparingMatcher implements PatternMatcher {

    private final BoyerMooreFromPattern boyerMoore;

    public BytesComparingMatcher(String pattern) {
        this.boyerMoore = BoyerMooreFromPattern.compilePattern(pattern);
    }

    public int match(byte[] bytes, long offset, String absolutePath) {
        int matches = 0;
        int match;
        int from = 0;
        long matchPosition;
        while (true) {
            match = boyerMoore.match(bytes, from, bytes.length);
            if (match == -1) {
                break;
            }
            from = match + boyerMoore.getPattern().length;
            matchPosition = offset + match;
            printMatch(absolutePath, matchPosition);
            matches++;
        }
        return matches;
    }

    public byte[] getPattern() {
        return boyerMoore.getPattern();
    }

    private void printMatch(String absolutePath, long matchPosition) {
        StringBuilder builder = new StringBuilder(absolutePath);
        builder.append(": ").append(matchPosition);
        System.out.println(builder.toString());
    }

}
