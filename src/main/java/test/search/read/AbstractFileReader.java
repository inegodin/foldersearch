package test.search.read;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;

/**
 * Created by inegodin on 03/05/15.
 */
public abstract class AbstractFileReader {

    public byte[] readBytesFrom(File file, long position, int amount) throws IOException {
        FileChannel channel = null;
        try {
            channel = new RandomAccessFile(file, "r").getChannel();
            return readBytesFrom(channel, position, amount);
        } finally {
            if (channel != null) {
                channel.close();
            }
        }
    }

    protected abstract byte[] readBytesFrom(FileChannel channel, long position, int amount) throws IOException;
}
