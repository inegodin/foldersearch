package test.search.read;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

/**
 * Created by inegodin on 03/05/15.
 */
public class WrappedBytesArrayFileReader extends AbstractFileReader {

    @Override
    protected byte[] readBytesFrom(FileChannel channel, long position, int amount) throws IOException {
        byte[] bytes = new byte[amount];
        ByteBuffer buffer = ByteBuffer.wrap(bytes);
        channel.read(buffer, position);
        return bytes;
    }

}
