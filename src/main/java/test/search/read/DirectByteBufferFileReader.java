package test.search.read;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

/**
 * Created by inegodin on 04/05/15.
 */
public class DirectByteBufferFileReader extends AbstractFileReader {

    @Override
    protected byte[] readBytesFrom(FileChannel channel, long position, int amount) throws IOException {
        ByteBuffer buffer = ByteBuffer.allocateDirect(amount);
        int read = channel.read(buffer, position);
        byte[] bytes = new byte[read];
        buffer.position(0);
        buffer.limit(read);
        buffer.get(bytes, 0, read);
        return bytes;
    }
}
