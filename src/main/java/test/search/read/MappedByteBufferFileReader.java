package test.search.read;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

/**
 * Created by inegodin on 04/05/15.
 */
public class MappedByteBufferFileReader extends AbstractFileReader {

    @Override
    protected byte[] readBytesFrom(FileChannel channel, long position, int amount) throws IOException {
        ByteBuffer buffer = channel.map(FileChannel.MapMode.READ_ONLY, position, amount);
        byte[] bytes = new byte[buffer.limit()];
        buffer.get(bytes, 0, buffer.limit());
        return bytes;
    }
}
