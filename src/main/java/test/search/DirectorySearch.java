package test.search;

import test.search.match.BytesComparingMatcher;
import test.search.match.PatternMatcher;
import test.search.read.AbstractFileReader;
import test.search.read.WrappedBytesArrayFileReader;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by inegodin on 03/05/15.
 */
public class DirectorySearch implements Search {

    private static final Logger LOGGER = Logger.getLogger(DirectorySearch.class.getName());
    private static final int BUFFER_SIZE = 256 * 1024;
    private final PatternMatcher patternMatcher;
    private final ExecutorService executorService;
    private final AbstractFileReader fileReader;
    private final ConcurrentHashMap<String, AtomicInteger> filesToMatches;
    private final List<Future<Integer>> tasks;
    private int submittedTaskCount;
    private long startTime;
    private long endTime;


    public DirectorySearch(int threads, String pattern) {
        if (threads < 1) {
            throw new IllegalArgumentException("Thread count must be equal at least to 1");
        }
        if (pattern == null || pattern.isEmpty()) {
            throw new IllegalArgumentException("Pattern must not be null or empty");
        }
        executorService = Executors.newFixedThreadPool(threads);
        patternMatcher = new BytesComparingMatcher(pattern);
        fileReader = new WrappedBytesArrayFileReader();
        filesToMatches = new ConcurrentHashMap<String, AtomicInteger>();
        tasks = new ArrayList<Future<Integer>>();
    }

    public int searchIn(String directory) {
        startTime = System.currentTimeMillis();
        try {
            File parent = isDirectoryValid(directory);
            if (parent == null) {
                LOGGER.severe("Given directory is invalid");
                return 0;
            }
            searchDirectory(parent);
            return getResults();
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "Exception during file read", e);
        } catch (InterruptedException e) {
            LOGGER.log(Level.SEVERE, "Exception during waiting results", e);
        } finally {
            executorService.shutdownNow();
            endTime = System.currentTimeMillis();
        }
        return 0;
    }

    @Override
    public int getMatchedFilesCount() {
        return filesToMatches.keySet().size();
    }

    @Override
    public long getUptime() {
        return endTime - startTime;
    }

    private int getResults() throws InterruptedException {
        int matchesCount = 0;
        for (int i = 0; i < submittedTaskCount; i++) {
            try {
                matchesCount += tasks.get(i).get();
            } catch (ExecutionException e) {
                LOGGER.log(Level.WARNING, "Exception during execution of search task", e);
            }
        }
        return matchesCount;
    }

    private void searchDirectory(File parent) throws IOException {
        File[] files = parent.listFiles();
        if (files != null) {
            for (File file : files) {
                if (file.canRead()) {
                    if (file.isDirectory()) {
                        searchDirectory(file);
                    } else {
                        submittedTaskCount += splitFileOnTasks(file);
                    }
                }
            }
        }
    }

    private File isDirectoryValid(String directory) {
        if (directory == null) {
            return null;
        }
        File parent = new File(directory);
        if (!(parent.exists() && parent.isDirectory())) {
            return null;
        }
        return parent;
    }

    private long splitFileOnTasks(File file) {
        long size = file.length();
        int patternLength = patternMatcher.getPattern().length - 1;
        if (size > BUFFER_SIZE) {
            long bufferCount = size / BUFFER_SIZE + 1;
            bufferCount = (size + bufferCount * patternLength) / BUFFER_SIZE + 1;
            long position = 0;
            long amount = BUFFER_SIZE;
            int positionIncrement = BUFFER_SIZE - patternLength;
            for (int i = 0; i < bufferCount; i++) {
                if (i != 0) {
                    position += positionIncrement;
                }
                if (i == bufferCount - 1) {
                    amount = size - position;
                }
                submitTask(file, position, (int) amount);
            }
            return bufferCount;
        } else {
            submitTask(file, 0, (int) size);
            return 1L;
        }
    }

    private void submitTask(File file, long offset, int amount) {
        tasks.add(executorService.submit(new SearchTask(file, offset, amount)));
    }

    private class SearchTask implements Callable<Integer> {

        private final File file;
        private final long offset;
        private final int amount;

        public SearchTask(File file, long offset, int amount) {
            this.file = file;
            this.offset = offset;
            this.amount = amount;
        }

        @Override
        public Integer call() throws Exception {
            String absolutePath = file.getAbsolutePath();
            byte[] bytes = fileReader.readBytesFrom(file, offset, amount);
            int matches = patternMatcher.match(bytes, offset, absolutePath);
            if (matches != 0) {
                AtomicInteger atomicInteger = filesToMatches.putIfAbsent(absolutePath, new AtomicInteger(matches));
                if (atomicInteger != null) {
                    atomicInteger.addAndGet(matches);
                }
            }
            return matches;
        }
    }
}
